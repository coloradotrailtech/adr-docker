  #!/bin/bash

docker build --no-cache -t backadr:1.0 ./back

docker build --no-cache -t frontadr:1.0 ./front

docker build --no-cache -t cronjob:1.0 ./cronjob

docker-compose up -d